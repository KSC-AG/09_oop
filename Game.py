from Player import *
from Kartenstapel import *

spiel = True

# Erzeugen eines Kartenhaufens
meinKartenhaufen = Kartenstapel()
meinKartenhaufen.mischen()

# Erzeugen Spieler
spielerlist = []
print("Wie viele Spieler spielen?")
anzahl = int(input("Anzahl: "))

for player in range(anzahl):
    print("Wie heißt du? ")
    name = input("Name: ")
    spielerlist.append(Spieler(name))

# ! Achtung: Das Spiel ist noch nicht fertig!
# Spielablauf:
while spiel:
    ausgesetzt = 0  # Ohne das würde das Spiel nicht enden, wenn alle Spieler aussetzen
    for player in spielerlist:
        if not player.gethand():  # Standardmäßig 2 Karten ziehen
            player.hinzufuegen(meinKartenhaufen.ziehen())
            player.hinzufuegen(meinKartenhaufen.ziehen())
        elif meinKartenhaufen.istLeer():
            print("Der Kartenstapel ist leer!")
            spiel = False
            break

    for player in spielerlist:
        if player.skipped or player.getwert() > 21 or not player.status:
            ausgesetzt += 1
            continue
        print("\n\n" + player.getname(), "ist an der Reihe!")
        print("Deine Karten: ", player.gethand())
        print("Dein Gesamtwert: ", player.getwert())

        if player.getwert() < 21:
            print("Ziehen, Aussetzen oder Spiel beenden? (z/a/b) ")
            ziehen = input("Ziehen, Aussetzen oder Spiel beenden?: ")
            if ziehen == "z":
                if meinKartenhaufen.istLeer():
                    print("Der Kartenstapel ist leer, du kannst keine Karte mehr ziehen!")
                    spiel = False
                    break
                else:
                    player.hinzufuegen(meinKartenhaufen.ziehen())
                    print("Deine Karten: ", player.gethand())
            elif ziehen == "a":
                player.skipped = True
                ausgesetzt += 1
                print("Du hast ausgesetzt")
                continue
            elif ziehen == "b":
                spiel = False
                break
            else:
                print("Falsche Eingabe")

        if player.getwert() > 21:
            print(player.getname(), "hat überzogen und ist raus!")
            player.status = False
            if not spielerlist:
                print("Alle Spieler haben überzogen!")
                spiel = False
                break
            else:
                continue
        if player.getwert() == 21:
            print("Du hast gewonnen!")
            spiel = False
            break

    if ausgesetzt == anzahl:
        print("Alle Spieler haben ausgesetzt!")
        spiel = False
        break

# Gewinner bestimmen
unterschied = float("inf")  # Startet mit unendlich, weil es noch keinen Unterschied gibt
gewinner = None

for player in spielerlist:
    if not player.status:
        continue
    differenz = abs(player.getwert() - 21)  # abs konvertiert Differenz in positive Zahl
    if differenz < unterschied:
        unterschied = differenz  # Wenn der Unterschied kleiner ist, wird er gespeichert
        gewinner = player  # Der Spieler mit dem kleinsten Unterschied wird als Gewinner gespeichert

if gewinner:
    print(f"\n\nDer Gewinner ist: {gewinner.getname()} mit {gewinner.getwert()} Punkten.")
