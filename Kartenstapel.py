import random


class Kartenstapel:
    def __init__(self):
        self.__karten = ['A♥', '2♥', '3♥', '4♥', '5♥', '6♥', '7♥', '8♥', '9♥', '10♥', 'J♥', 'Q♥', 'K♥',
                         'A♦', '2♦', '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '10♦', 'J♦', 'Q♦', 'K♦',
                         'A♣', '2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '10♣', 'J♣', 'Q♣', 'K♣',
                         'A♠', '2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '10♠', 'J♠', 'Q♠', 'K♠']

    def mischen(self):
        random.shuffle(self.__karten)

    def ziehen(self):
        if not self.istLeer():
            return self.__karten.pop()
        else:
            print("Der Kartenstapel ist leer")

    def istLeer(self):
        return len(self.__karten) == 0

    def get_karten(self):
        return self.__karten

    def set_karten(self, karten):  # Nicht verwenden!
        self.__karten = karten
