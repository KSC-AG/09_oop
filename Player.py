class Spieler:
    def __init__(self, name):
        self.__name = name
        self.__hand = []
        self.__wert = 0
        self.skipped = False
        self.status = True

    def hinzufuegen(self, karte):
        if karte:
            self.__hand.append(karte)
            self.__wert += self.kartenwert(karte)

    def gethand(self):
        return self.__hand

    def getwert(self):
        return self.__wert

    def kartenwert(self, karte):
        if karte[0] == 'A':
            return 11
        elif karte[0] in 'JQK' or karte[:2] == '10':
            return 10
        else:
            return int(karte[:-1])

    def getname(self):
        return self.__name